#!/usr/bin/env python
from pathlib import Path
import subprocess


def make_pngs(dest_dir: Path, ink_file: Path):
    w = float(subprocess.check_output(['inkscape', '-W',
                                       str(ink_file)])
              .decode('utf8'))
    h = float(subprocess.check_output(['inkscape', '-H',
                                       str(ink_file)])
              .decode('utf8'))
    size_arg = '-h' if h < w else '-w'

    for min_size in [*range(100, 501, 100), 1000]:
        output = dest_dir / f'{ink_file.stem}_{min_size}.png'
        subprocess.run(['inkscape',
                        '-e', str(output),
                        size_arg, str(min_size),
                        str(ink_file)])


def make_pdf(dest_dir: Path, ink_file: Path):
    output = dest_dir / f'{ink_file.stem}.pdf'
    subprocess.run(['inkscape',
                    '-A', str(output),
                    str(ink_file)])


def make_svg(dest_dir: Path, ink_file: Path):
    output = dest_dir / f'{ink_file.stem}.svg'
    subprocess.run(['inkscape',
                    '-l', str(output),
                    str(ink_file)])


def make_eps(dest_dir: Path, ink_file: Path):
    output = dest_dir / f'{ink_file.stem}.eps'
    subprocess.run(['inkscape',
                    '-E', str(output),
                    str(ink_file)])


def make_renders(renderers: list):
    for p in Path('inkscape/').glob('*/*.svg'):
        dest_dir = Path(p.parent.name)
        if not dest_dir.exists():
            dest_dir.mkdir()

        for renderer in renderers:
            renderer(dest_dir, p)


if __name__ == '__main__':
    make_renders([make_pngs, make_svg, make_pdf, make_eps])
