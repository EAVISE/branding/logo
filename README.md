![EAVISE header image](twitter/header_500.png)

This repository bundles several variants of the EAVISE logo. The [color](color)
directory contains the variants in their standard colors.
There are 4 variants of the logo:

## `logo_eavise_tag`
The logo with text and a tagline ([svg](color/logo_eavise_tag.svg), [png](color/logo_eavise_tag_300.png), [pdf](color/logo_eavise_tag.pdf), [eps](color/logo_eavise_tag.eps))

**Note:** The example below is the **lowest resolution** PNG. Consider using one of the **higher resolutions**:
[200px](color/logo_eavise_tag_200.png),
[300px](color/logo_eavise_tag_300.png),
[400px](color/logo_eavise_tag_400.png),
[500px](color/logo_eavise_tag_500.png)

![Logo EAVISE with tagline](color/logo_eavise_tag_100.png)

## `logo_eavise`
The logo with text but without the tagline ([svg](color/logo_eavise.svg), [png](color/logo_eavise_300.png), [pdf](color/logo_eavise.pdf), [eps](color/logo_eavise.eps))

**Note:** The example below is the **lowest resolution** PNG. Consider using one of the 
**higher resolutions**:
[200px](color/logo_eavise_200.png),
[300px](color/logo_eavise_300.png),
[400px](color/logo_eavise_400.png),
[500px](color/logo_eavise_500.png)

![Logo EAVISE without tagline](color/logo_eavise_100.png)

## `icon_eavise`
The icon itself, without any text ([svg](color/icon_eavise.svg), [png](color/icon_eavise_300.png), [pdf](color/icon_eavise.pdf), [eps](color/icon_eavise.eps))

**Note:** The example below is the **lowest resolution** PNG. Consider using one of the 
**higher resolutions**:
[200px](color/icon_eavise_200.png),
[300px](color/icon_eavise_300.png),
[400px](color/icon_eavise_400.png),
[500px](color/icon_eavise_500.png)

![Logo icon EAVISE](color/icon_eavise_100.png)

## `kul_eavise`
The logo combined with the KU Leuven logo. This can for example be used in presentations. ([svg](color/kul_eavise.svg), [png](color/kul_eavise_300.png), [pdf](color/kul_eavise.pdf), [eps](color/kul_eavise.eps))

**Note:** The example below is the **lowest resolution** PNG. Consider using one of the 
**higher resolutions**:
[200px](color/kul_eavise_200.png),
[300px](color/kul_eavise_300.png),
[400px](color/kul_eavise_400.png),
[500px](color/kul_eavise_500.png)

![Logo EAVISE with KUL logo](color/kul_eavise_100.png)

# Four extensions
The 4 variants come in 4 extensions: `*.svg`, `*.pdf`, `*.png` and `*.eps`.
It is advised to **always use a vector-based** version of the logo (`*.svg` or `*.eps`) when possible. The `*.png` files come in multiple heights. The height of a `*.png` file is contained in the file name, e.g. `icon_eavise_100.png` has a height of 100px.

# Black or white?
Apart from the variants mentioned above, the subdirectories [black](black) and [white](white) contain the same variants of the logo but in a black and a white version respectively. All these variants also adhere to the same naming convention as previously described.

Some examples:

* The logo in **black** with a tagline ([svg](black/logo_eavise_tag.svg), [png](black/logo_eavise_tag_300.png), [pdf](black/logo_eavise_tag.pdf), [eps](black/logo_eavise_tag.eps))

**Note:** The example below is the **lowest resolution** PNG. Consider using one of the 
**higher resolutions**:
[200px](black/logo_eavise_tag_200.png),
[300px](black/logo_eavise_tag_300.png),
[400px](black/logo_eavise_tag_400.png),
[500px](black/logo_eavise_tag_500.png)

![Logo EAVISE black](black/logo_eavise_tag_100.png)

* The logo in **black** without a tagline ([svg](black/logo_eavise.svg), [png](black/logo_eavise_300.png), [pdf](black/logo_eavise.pdf), [eps](black/logo_eavise.eps))

**Note:** The example below is the **lowest resolution** PNG. Consider using one of the 
**higher resolutions**:
[200px](black/logo_eavise_200.png),
[300px](black/logo_eavise_300.png),
[400px](black/logo_eavise_400.png),
[500px](black/logo_eavise_500.png)

![Logo EAVISE black](black/logo_eavise_100.png)

* The logo in **white** with a tagline ([svg](white/logo_eavise_tag.svg), [png](white/logo_eavise_tag_300.png), [pdf](white/logo_eavise_tag.pdf), [eps](white/logo_eavise_tag.eps)). Probably hard to see on a white background...

**Note:** The example below is the **lowest resolution** PNG. Consider using one of the 
**higher resolutions**:
[200px](white/logo_eavise_200.png),
[300px](white/logo_eavise_300.png),
[400px](white/logo_eavise_400.png),
[500px](white/logo_eavise_500.png)

![Logo EAVISE black](white/logo_eavise_tag_100.png)

* The logo in **white** without a tagline ([svg](white/logo_eavise.svg), [png](white/logo_eavise_300.png), [pdf](white/logo_eavise.pdf), [eps](white/logo_eavise.eps)). Probably hard to see on a white background...

**Note:** The example below is the **lowest resolution** PNG. Consider using one of the 
**higher resolutions**:
[200px](white/logo_eavise_200.png),
[300px](white/logo_eavise_300.png),
[400px](white/logo_eavise_400.png),
[500px](white/logo_eavise_500.png)

![Logo EAVISE black](white/logo_eavise_100.png)

# E-mail signature
We created an e-mail signature based on the KU Leuven default signature but with the EAVISE logo.
You can download it [here](https://gitlab.com/EAVISE/logo/raw/master/email/signature.html?inline=false).

Edit the html file in any text editor to replace the example with your professional information. The example signature looks like this:

![Example e-mail signature](email/signature.png)

# Social media
We also created some variants of the logo for our social media pages. These include several banner/cover/header images

## Profile picture
Available resolutions:
[100px](facebook/profile_100.png),
[200px](facebook/profile_200.png),
[300px](facebook/profile_300.png),
[400px](facebook/profile_400.png),
[500px](facebook/profile_500.png).

![EAVISE profile picture](facebook/profile_300.png)

## Facebook cover
Available resolutions:
[100px](facebook/cover_100.png),
[200px](facebook/cover_200.png),
[300px](facebook/cover_300.png),
[400px](facebook/cover_400.png),
[500px](facebook/cover_500.png).

![EAVISE Facebook cover](facebook/cover_500.png)

## LinkedIn cover
Available resolutions:
[100px](linkedin/cover_100.png),
[200px](linkedin/cover_200.png),
[300px](linkedin/cover_300.png),
[400px](linkedin/cover_400.png),
[500px](linkedin/cover_500.png).

![EAVISE LinkedIn cover](linkedin/cover_500.png)

## Twitter header
Available resolutions:
[100px](twitter/header_100.png),
[200px](twitter/header_200.png),
[300px](twitter/header_300.png),
[400px](twitter/header_400.png),
[500px](twitter/header_500.png).

![EAVISE Twitter header](twitter/header_500.png)

## Vertical banner
This might be useful for marketing banners that stand up straight.

Available resolutions:
[100px](vertical_banner/vertical_banner_100.png)
[200px](vertical_banner/vertical_banner_200.png)
[300px](vertical_banner/vertical_banner_300.png)
[400px](vertical_banner/vertical_banner_400.png)
[500px](vertical_banner/vertical_banner_500.png)
[1000px](vertical_banner/vertical_banner_1000.png)

![EAVISE vertical banner](vertical_banner/vertical_banner_500.png)

# The font
The eavise logo is created with a font called [Electrolize](https://fonts.google.com/specimen/Electrolize). This font is included in the [font](font) directory, along with its Open Font License.

# ASCII
Because we can, we created an ASCII variant of the logo. It is included in the [ascii](ascii) folder. The `*_color.txt` version uses ANSI colors.

```
                           eeee                       
                        eeeeeeeeee                    
                    eeeeeeeeeeeeeeeeee                
                 eeeeeeeeeeeeeeeeeeeeeeee             
             eeeeeeeeeeee        eeeeeeeeeeee         
          eeeeeeeeeeee              eeeeeeeeeeee      
       eeeeeeeeeee                      eeeeeeeeeeee  
     eeeeeeeeee                         eeeeeeeeeee   
    eeeeeeee                         eeeeeeeeeee      
    eeeeeee             eeeeeeeeeeeeeeeeeeee          
    eeeeee           eeeeeeeeeeeeeeeeeeee             
    eeeeee          eeeee     eeeeeeeee           eeee
    eeeeee         eeeee       eeeeeeee         eeeeee
    eeeeee        eeeeeee     eeeeeeeeee        eeeeee
    eeeeee        eeeeeeeeeeeeeeeeeeeeee        eeeeee
    eeeeee         eeeeeeeeeeeeeeeeeeee         eeeeee
    eeeeee          eeeeeeeeeeeeeeeeee          eeeeee
    eeeeee           eeeeeeeeeeeeeeee           eeeeee
    eeeeeee             eeeeeeeeee             eeeeeee
    eeeeeeee                                  eeeeeeee
     eeeeeeeeee                            eeeeeeeeee 
       eeeeeeeeeee                      eeeeeeeeeee   
          eeeeeeeeeeee              eeeeeeeeeeee      
             eeeeeeeeeeee       eeeeeeeeeeeee         
                 eeeeeeeeeeeeeeeeeeeeeeee             
                    eeeeeeeeeeeeeeeeee                
                        eeeeeeeeee                    
                           eeee
```                       

# Presentations and posters
We created the [Beavise LaTeX presentation template](https://gitlab.com/EAVISE/beavise) that tries to combine the classic KU Leuven presentation template
with the minimalistic look and feel of the EAVISE logo. Feel free to give it a try! It also works for posters.